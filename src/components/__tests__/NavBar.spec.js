import { describe, it, expect } from "vitest";

import { mount } from "@vue/test-utils";
import Navbar from "../NavBar.vue";
describe("HelloWorld", () => {
  it("renders properly", () => {
    const wrapper = mount(Navbar);
    expect(wrapper.text()).toContain("Home");
  });
});
